# Simple App Deployment with GitLab CI/CD - [Open App](http://3.89.228.177/)

This repository contains the source code and configuration files for deploying a simple web app using GitLab CI/CD. The deployment process involves building a Docker image of the web app, pushing it to Docker Hub, and deploying it to an AWS EC2 instance.

## Overview

1. **Repository Creation**: 
   - A GitLab repository was created to manage the source code and configuration files.

2. **Web App Development**:
   - A basic HTML page was created, allowing users to input their name and display a greeting message.
   - The HTML page was tested locally to ensure functionality.

3. **Docker Configuration**:
   - A Dockerfile was created to containerize the web app using the Apache HTTP Server image.
   - The Dockerfile specifies how to build and run the web app within a Docker container.
   - The Dockerfile was added to the repository.

4. **GitLab CI/CD Configuration**:
   - A GitLab CI/CD pipeline was configured using a `.gitlab-ci.yml` file.
   - The pipeline consists of multiple stages:
     - Test: Linting of HTML files using htmlhint.
     - Build: Building the Docker image of the web app and pushing it to Docker Hub.
     - Deploy: Deploying the Docker image to an AWS EC2 instance.

5. **Docker Hub Repository Creation**:
   - A Docker Hub repository was created to store the Docker image of the web app.

6. **AWS EC2 Instance Configuration**:
   - An Ubuntu AWS EC2 instance was provisioned with Docker installed.
   - The necessary security group rules were configured to allow inbound traffic on port 80.

7. **Deployment**:
   - The GitLab CI/CD pipeline automatically tests, builds, and deploys the web app.
   - Upon successful deployment, the web app becomes accessible via the public IP of the AWS EC2 instance on port 80.

## Contributors

- [Lakshya Kumar Sirohi](https://gitlab.com/sirohiLakshya)

Feel free to contribute to this project by submitting pull requests or opening issues.
