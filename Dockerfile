# Use the official Apache HTTP Server image
FROM httpd:latest

# Copy the HTML file into the container
COPY index.html /usr/local/apache2/htdocs/

# Expose port 80 to the outside world
EXPOSE 80

